.. geodat_t documentation master file, created by
   sphinx-quickstart on Sun Apr 28 17:02:46 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to geodat_t's documentation!
====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

    notebook/Untitled


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
